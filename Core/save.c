/**
 * \file save.c
 * \brief Routines used for save functionalities 
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "save.h"

static void mkdir_data(void)
{
	if (access("Data", F_OK) == -1) {
		mkdir("Data", 0700);
	}
}

static char * get_valid_save_name(char * msg)
{
	char * name;
	do {
		name = secure_inputstr(msg);
	} while (strlen(name) == 0);
	return name;
}

VECTOR_AT(Cell, Cell)

static void fwrite_game(const VectorCell * const coreBoard, const Stat stat, FILE * const save)
{
	if (
		(fwrite(&coreBoard->capacity, sizeof(size_t), 1, save) != 1) ||
		(fwrite(VectorCell_at(coreBoard, 0), sizeof(Cell), coreBoard->capacity, save) != coreBoard->capacity) ||
		(fwrite(&stat, sizeof(Stat), 1, save) != 1)
	) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

void Game_save(const Game game)
{
	FILE * save;
	char * path, * name;
	path = name = NULL;
	mkdir_data();
	name = get_valid_save_name("Enter a save name : ");
	if ((path = malloc((strlen(name) + 6) * sizeof(char))) == NULL) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	sprintf(path, "Data/%s", name);
	if ((save = fopen(path, "wb")) == NULL) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	fwrite_game(game.coreBoard, game.stat, save);
	free(name);
	free(path);
	if (fclose(save) == EOF) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

char * get_valid_load_name(void)
{
	char * name, * path;
	int acc;
	name = path = NULL;
	do {
		free(name);
		free(path);
		name = secure_inputstr("Load a save by entering its name or press enter to skip it : ");
		if ((path = malloc((strlen(name) + 6) * sizeof(char))) == NULL) {
			fprintf(stderr, "error : %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		sprintf(path, "Data/%s", name);
		acc = access(path, R_OK);
	} while ((acc == -1) && (strlen(name) != 0));
	free(name);
	return path;
}

VECTOR_MALLOC(Cell, Cell);

static Game fread_game(FILE * const load)
{
	Game game;
	size_t capacity;
	if (fread(&capacity, sizeof(size_t), 1, load) != 1) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	game.coreBoard = VectorCell_malloc(capacity);
	if (
		(fread(VectorCell_at(game.coreBoard, 0), sizeof(Cell), capacity, load) != capacity) ||
		(fread(&game.stat, sizeof(Stat), 1, load) != 1)
	) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return game;
}

Game Game_load(void)
{
	Game game;
	FILE * load;
	char * path;
	game.coreBoard = NULL;
	path = get_valid_load_name();
	if (strlen(path) > 5) {
		if ((load = fopen(path, "rb")) == NULL) {
			fprintf(stderr, "error : %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		game = fread_game(load);
		free(path);
		if (fclose(load) == EOF) {
			fprintf(stderr, "error : %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	return game;
}

void Stat_save(const Stat stat)
{
	FILE * save;
	time_t t;
	time(&t);
	if ((save = fopen("statistics", "ab")) == NULL) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	if (
	(fwrite(&t, sizeof(time_t), 1, save) != 1) ||
	(fwrite(&stat, sizeof(Stat), 1, save) != 1)
	) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	if (fclose(save) == EOF) {
		fprintf(stderr, "error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

void Stat_load(void)
{
	FILE * load;
	Stat stat;
	time_t t;
	if ((load = fopen("statistics", "r")) == NULL) {
		printf("You have to finish the first game for statistics to show up here.\n");
	} else {
		while (feof(load) == 0) {
			if (
			(fread(&t, sizeof(time_t), 1, load) != 1) ||
			(fread(&stat, sizeof(Stat), 1, load) != 1)
			) {
				if (ferror(load) != 0) {
					fprintf(stderr, "error : %s\n", strerror(errno));
					exit(EXIT_FAILURE);
				}
			} else {
				Stat_print(t, stat);
			}
		}
		if (fclose(load) == EOF) {
			fprintf(stderr, "error : %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
}
