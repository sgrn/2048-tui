/**
 * \file game.h
 * \brief Headers related to routines used for game data structures initialization.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef GAME_H
	#define GAME_H

	#include <stdlib.h>
	#include <time.h>

	#include "coreboard.h"
	#include "terminal_io.h"
	#include "save.h"
	#include "stat.h"
	#include "../Lib/Vector/vector.h"

	/**
	 * \fn int Game_get_board_side_length(void)
	 * \brief Ask user to choose a side length.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:53 AM
	 * \return Side length.
	 */
	int Game_get_board_side_length(void);

	/**
	 * \fn Game Game_start(void)
	 * \brief Initializes game data structures.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:53 AM
	 * \return \a Game object.
	 */
	Game Game_start(void);

	/**
	 * \fn void Game_end(Game * const game)
	 * \brief Frees memory taken by a given \a Game object and save end game stats.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:53 AM
	 * \param game Address of a \a Game object to free.
	 */
	void Game_end(Game * const game);

#endif
