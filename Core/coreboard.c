/**
 * \fn coreboard.c
 * \brief Routines operating on the backend board data structure.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 03:10 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "coreboard.h"

VECTOR_MALLOC(Cell, Cell)

VECTOR_SET(Cell, Cell)

VectorCell * VectorCell_allocinit(const size_t size)
{
	VectorCell * board;
	size_t length, i;
	srand(time(NULL));
	length = size * size;
	board = VectorCell_malloc(length);
	for (i = 0; i < length; i++) {
		VectorCell_set(board, i, 0);
	}
	return board;
}

VECTOR_GET(Cell, Cell)

VECTOR_AT(Cell, Cell)

static SubVectorCell SubVectorCell_extract(const size_t start, const ssize_t step, const size_t end, const VectorCell * const board)
{
	SubVectorCell sub;
	sub.start = VectorCell_at(board, start);
	sub.end = VectorCell_at(board, end);
	sub.step = step;
	sub.length = (labs(end - start) / labs(step)) + 1;
	return sub;
}

static Cell * SubVectorCell_at(const SubVectorCell sub, const size_t index)
{
	return sub.start + index * sub.step;
}

static Cell SubVectorCell_get(const SubVectorCell sub, const size_t index)
{
	return *SubVectorCell_at(sub, index);
}

static void SubVectorCell_set(const SubVectorCell sub, const size_t index, const Cell value)
{
	*SubVectorCell_at(sub, index) = value;
}

static SubVectorCell SubSubVectorCell_extract(const size_t start, const ssize_t step, const size_t end, const SubVectorCell sub)
{
	SubVectorCell subsub;
	subsub.start = SubVectorCell_at(sub, start);
	subsub.end = SubVectorCell_at(sub, end);
	subsub.step = step * sub.step;
	subsub.length = (labs(end - start) / labs(step)) + 1;
	return subsub;
}

static CursorCell CursorCell_init(const SubVectorCell sub)
{
	CursorCell cursor;
	cursor.position = 0;
	cursor.subVector = sub;
	return cursor;
}

static CursorCell CursorCell_next(const CursorCell cursor)
{
	CursorCell next;
	size_t nextPosition;
	next.subVector = cursor.subVector;
	nextPosition = cursor.position + 1;
	if (nextPosition <= cursor.subVector.length) {
		next.position = nextPosition;
	}
	return next;
}

static Cell CursorCell_get(const CursorCell cursor)
{
	return SubVectorCell_get(cursor.subVector, cursor.position);
}

static Cell * CursorCell_at(const CursorCell cursor)
{
	return SubVectorCell_at(cursor.subVector, cursor.position);
}

static void CursorCell_set(const CursorCell cursor, const Cell value)
{
	SubVectorCell_set(cursor.subVector, cursor.position, value);
}

static bool CursorCell_is_atend(const CursorCell cursor)
{
	return cursor.position >= cursor.subVector.length;
}

static CursorCell CursorCell_next_none_zero(CursorCell cursor)
{
	while ((! CursorCell_is_atend(cursor)) && (CursorCell_get(cursor) == 0)) {
		cursor = CursorCell_next(cursor);
	}
	return cursor;
}

STACK_PUSH(CursorCell, CursorCell)

static void StackCursorCell_push_cursor(StackCursorCell * const stack, const CursorCell cursor0, const CursorCell cursor1)
{
	if ((! CursorCell_is_atend(cursor1)) && 
	(CursorCell_get(cursor1) != 0)) {
		StackCursorCell_push(stack, cursor1);
	}
	if ((! CursorCell_is_atend(cursor0)) && 
	(CursorCell_get(cursor0) != 0)) {
		StackCursorCell_push(stack, cursor0);
	}
}

static bool SubVectorCell_group(SubVectorCell sub, Cell * previous, StackCursorCell * const stack, unsigned int * const points)
{
	CursorCell cursor0, cursor1, cursor2;
	SubVectorCell subsub;
	ssize_t offset;
	bool isWinner;
	cursor0 = CursorCell_init(sub);
	cursor0 = CursorCell_next_none_zero(cursor0);
	cursor1 = CursorCell_next(cursor0);
	cursor1 = CursorCell_next_none_zero(cursor1);
	isWinner = false;
	if (! CursorCell_is_atend(cursor1)) {
		cursor2 = cursor1;
		if (CursorCell_get(cursor0) == CursorCell_get(cursor1)) {
			CursorCell_set(cursor0, 0);
			CursorCell_set(cursor1, 2 * CursorCell_get(cursor1));
			*points += CursorCell_get(cursor1);
			isWinner = CursorCell_get(cursor1) == 2048;
			cursor2 = CursorCell_next(cursor1);
		}
		offset = sub.length - 1 - cursor2.position;
		if (offset >= 0) {
			subsub = SubSubVectorCell_extract(cursor2.position, 1, sub.length - 1, sub);
			isWinner = isWinner || SubVectorCell_group(subsub, CursorCell_at(cursor1), stack, points);
		}
	}
	if (previous == CursorCell_at(cursor0)) {
		cursor0.position = sub.length;
	}
	StackCursorCell_push_cursor(stack, cursor0, cursor1);
	return isWinner;
}

STACK_ISEMPTY(CursorCell, CursorCell)

STACK_GET_TOP(CursorCell, CursorCell)

STACK_POP(CursorCell, CursorCell)

static void SubVectorCell_shift(SubVectorCell sub, StackCursorCell * const stack)
{
	size_t i;
	CursorCell cursor;
	Cell value;
	i = 0;
	while (! StackCursorCell_isempty(stack)) {
		cursor = StackCursorCell_get_top(stack);
		value = CursorCell_get(cursor);
		CursorCell_set(cursor, 0);
		SubVectorCell_set(sub, i, value);
		StackCursorCell_pop(stack);
		i++;
	}
}

STACK_MALLOC(CursorCell, CursorCell)

STACK_FREE(CursorCell, CursorCell)

static bool SubVectorCell_swipe(SubVectorCell sub, StackCursorCell * stack, unsigned int * const points)
{
	SubVectorCell reversed;
	bool isWinner;
	stack = StackCursorCell_malloc();
	reversed = SubSubVectorCell_extract(sub.length - 1, -1, 0, sub);
	isWinner = SubVectorCell_group(reversed, NULL, stack, points);
	SubVectorCell_shift(reversed, stack);
	return isWinner;
}

#define periodic_binary_signal pbs

static bool periodic_binary_signal(const size_t n, const size_t ht)
{
	return (1 + powf(-1, n / ht)) / 2;
}

static SubVectorCell SubVectorCell_extract_before_swiping(const CoreboardAction action, const size_t i, const size_t length, VectorCell * const board)
{
	size_t startSubAt, endSubAt, end, bottom;
	ssize_t step;
	bool yAxisOperation, xAxisOperation;
	end = length - 1;
	bottom = length * end;
	xAxisOperation = pbs(action, 2);
	yAxisOperation = pbs(action + 2, 2);
	startSubAt = xAxisOperation * (i * length + action * end) + yAxisOperation * (i + (action - 2) * bottom);
	step = powf((-1), action % 2) * (1 + (action / 2) * end);
	endSubAt = xAxisOperation * (startSubAt + powf(-1, action) * end) + yAxisOperation * (i + ((action + 1) % 2) * bottom);
	return SubVectorCell_extract(startSubAt, step, endSubAt, board);
}

bool VectorCell_swipe(VectorCell * const board, const CoreboardAction action, unsigned int * const points)
{
	StackCursorCell * stack;
	SubVectorCell sub;
	size_t i, length;
	bool isWinner;
	stack = StackCursorCell_malloc();
	length = sqrtf(board->capacity);
	isWinner = false;
	for (i = 0; i < length; i++) {
		sub = SubVectorCell_extract_before_swiping(action, i, length, board);
		isWinner = isWinner || SubVectorCell_swipe(sub, stack, points);
	}
	StackCursorCell_free(&stack);
	return isWinner;
}

static ssize_t draw_index(const ssize_t min, const ssize_t max)
{
	return rand() % (max + 1 - min) + min;
}

void VectorCell_insert_randomly(VectorCell * const board)
{
	size_t i, j, randomIndex;
	Cell value;
	value = draw_index(1, 2) * 2;
	i = 0;
	randomIndex = draw_index(i, board->capacity - 1);
	while ((i < board->capacity) &&
	(VectorCell_get(board, i) != 0) &&
	(VectorCell_get(board, randomIndex) != 0)) {
		randomIndex = draw_index(i, board->capacity - 1);
		i++;
	}
	j = i;
	while ((j < board->capacity) && (VectorCell_get(board, randomIndex) != 0)) {
		randomIndex = draw_index(i, board->capacity - 1);
		j++;
	}
	if (VectorCell_get(board, randomIndex) == 0) {
		VectorCell_set(board, randomIndex, value);
	} else if (i < board->capacity) {
		VectorCell_set(board, i, value);
	}
}

static bool SubVectorCell_is_subset_movable(const SubVectorCell sub[2])
{
	bool isMovable;
	size_t i;
	Cell a, b;
	isMovable = false;
	i = 0;
	while ((i < 4) && (! isMovable)) {
		a = SubVectorCell_get(sub[pbs(i + 2, 2)], pbs(i + 3, 2));
		b = SubVectorCell_get(sub[pbs(i + 3, 2)], pbs(i, 2));
		isMovable = (a == b) || (a == 0);
		i++;
	}
	return isMovable;
}

bool VectorCell_has_lost(VectorCell * const board)
{
	bool hasLost;
	size_t length, i, j, start0, start1, end0, end1;
	SubVectorCell sub[2];
	hasLost = true;
	length = sqrtf(board->capacity) - 1;
	i = 0;
	while ((i < length) && hasLost) {
		j = 0;
		while ((j < length) && hasLost) {
			start0 = i * (length + 1) + j;
			end0 = start0 + 1;
			start1 = (i + 1) * (length + 1) + j;
			end1 = start1 + 1;
			sub[0] = SubVectorCell_extract(start0, 1, end0, board);
			sub[1] = SubVectorCell_extract(start1, 1, end1, board);
			hasLost = ! SubVectorCell_is_subset_movable(sub);
			j++;
		}
		i++;
	}
	return hasLost;
}

static bool SubVectorCell_is_action_valid(SubVectorCell sub)
{
	bool isActionValid;
	Cell current, next;
	size_t i;
	isActionValid = false;
	i = 0;
	sub.length--;
	while ((i < sub.length) && (! isActionValid)) {
		current = SubVectorCell_get(sub, i);
		next = SubVectorCell_get(sub, i + 1);
		isActionValid = (current != 0) && ((next == 0) || (next == current));
		i++;
	}
	return isActionValid;
}

bool VectorCell_is_action_valid(VectorCell * const board, const CoreboardAction action)
{
	SubVectorCell sub;
	size_t i, length;
	bool isActionValid;
	length = sqrtf(board->capacity);
	isActionValid = false;
	i = 0;
	while ((i < length) && (! isActionValid)) {
		sub = SubVectorCell_extract_before_swiping(action, i, length, board);
		isActionValid = SubVectorCell_is_action_valid(sub);
		i++;
	}
	return isActionValid;
}
