/**
 * \file stat.h
 * \brief Headers related to routines used for statistics calculation.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef STAT_H
	#define STAT_H

	#include <time.h>
	#include <stdio.h>
	#include <errno.h>

	#include "coreboard.h"

	/**
	 * \struct Stat
	 * \date 2020-06-14 Sun 12:25 AM
	 * \brief This structure represents game related data structure.
	 * \var score
	 * Current number of points.
	 * \var averagePointsPerMove
	 * Average points made per move.
	 * \var motionCounter
	 * Current number total number of moves.
	 * \var directedMotionCounter
	 * Current number of moves in a specific direction.
	 * \var totalTime
	 * Duration of a game.
	 * \var averageTimeBetweenMoves
	 * Player average response time.
	 */
	struct Stat
	{
		unsigned int score;
		float averagePointsPerMove;
		unsigned short motionCounter;
		unsigned short directedMotionCounter[4];
		time_t totalTime;
		double averageTimeBetweenMoves;
	};

	typedef struct Stat Stat;

	/**
	 * \fn void Stat_print(const time_t t, const Stat stat)
	 * \brief Prints given stats to the screen.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 06:57 AM
	 * \param t Current time.
	 * \param stat Current stats.
	 */
	void Stat_print(const time_t t, const Stat stat);

	/**
	 * \fn double Stat_get_time(void)
	 * \brief get current nanosecond time.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 06:57 AM
	 * \return Nanosecond time.
	 */
	double Stat_get_time(void);

	/**
	 * \fn void Stat_refresh(Stat * const stat, const double diffTime, const unsigned short diffPts, CoreboardAction action)
	 * \brief Refreshes a given \a Stat object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 06:57 AM
	 * \param stat Address to the \a Stat object to refresh.
	 * \param diffTime Player current response time.
	 * \param diffPts Newly earned points.
	 * \param action A \a CoreboardAction user-level action
	 */
	void Stat_refresh(Stat * const stat, const double diffTime, const unsigned short diffPts, CoreboardAction action);

	/**
	 * \fn Stat Stat_init(void)
	 * \brief Returns an initialized \a Stat object value.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 06:57 AM
	 * \return Initialized \a Stat object value.
	 */
	Stat Stat_init(void);

#endif
