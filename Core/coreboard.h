/**
 * \file coreboard.h
 * \brief Headers related to routines operating on the backend board data structure.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef COREBOARD_H
	#define COREBOARD_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <errno.h>
	#include <math.h>
	#include <time.h>

	#include "../Lib/Vector/vector.h"
	#include "../Lib/Stack/stack.h"

	typedef unsigned short Cell;

	VECTOR(Cell, Cell)

	/**
	 * \struct SubVectorCell
	 * \date 2020-06-14 Sun 12:25 AM
	 * \brief This structure type represents sub vectors by references.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \var start
	 * Start address.
	 * \var end
	 * End address.
	 * \var step
	 * Offset between elements.
	 * \var length
	 * Number of elements.
	 */
	struct SubVectorCell
	{
		Cell * start;
		Cell * end;
		ssize_t step;
		size_t length;
	};

	typedef struct SubVectorCell SubVectorCell;

	/**
	 * \struct CursorCell
	 * \date 2020-06-14 Sun 12:25 AM
	 * \brief This structure type represents a cursor tied to a specific sub vector.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \var position
	 * Current cursor position.
	 * \var subVector
	 * Sub vector tied to the cursor.
	 */
	struct CursorCell
	{
		size_t position;
		SubVectorCell subVector;
	};

	typedef struct CursorCell CursorCell;

	STACK(CursorCell, CursorCell)

	/**
	 * \enum CursorCell
	 * \date 2020-06-14 Sun 12:25 AM
	 * \brief This enumeration represents all doable user-level actions on \a VectorCell objects.
	 */
	enum CoreboardAction
	{
		COREBOARD_RIGHT, /**< RIGHT OPERATION*/
		COREBOARD_LEFT, /**< LEFT OPERATION*/
		COREBOARD_DOWN, /**< DOWN OPERATION*/
		COREBOARD_UP, /**< UP OPERATION*/
		COREBOARD_NONE /**< NO OPERATIONS*/
	};

	typedef enum CoreboardAction CoreboardAction;

	/**
	 * \fn VectorCell * VectorCell_allocinit(const size_t size)
	 * \brief Allocates and initializes a \a VectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:03 PM
	 * \param size Square root of the final \a VectorCell size.
	 * \return Newly allocated and initialized \a VectorCell object.
	 * A \a VectorCell object is a 2 dimensional linearized vector that contains Cell typed data.
	 */
	VectorCell * VectorCell_allocinit(const size_t size);

	/**
	 * \fn static SubVectorCell SubVectorCell_extract(const size_t start, const ssize_t step, const size_t end, const VectorCell * const board)
	 * \brief Extracts a \a SubVectorCell object from a regular \a VectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:05 PM
	 * \param start Index corresponding to the \a SubVectorCell first element.
	 * \param step Offset between \a SubVectorCell elements (relative to a given \a VectorCell object).
	 * \param end Index corresponding to the \a SubVectorCell last element.
	 * \param board Given \a VectorCell object on which to extract the \a SubVectorCell object.
	 * \return Extracted \a SubVectorCell object.
	 */
	static SubVectorCell SubVectorCell_extract(const size_t start, const ssize_t step, const size_t end, const VectorCell * const board);

	/**
	 * \fn static Cell * SubVectorCell_at(const SubVectorCell sub, const size_t index)
	 * \brief Gets address of \a SubVectorCell element of a given index.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:06 PM
	 * \param sub Given \a SubVectorCell object.
	 * \param index Given index of a \a SubVectorCell element.
	 * \return Address of \a SubVectorCell element of a given index.
	 */
	static Cell * SubVectorCell_at(const SubVectorCell sub, const size_t index);

	/**
	 * \fn static Cell SubVectorCell_get(const SubVectorCell sub, const size_t index)
	 * \brief Gets value of \a SubVectorCell element of a given index.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:07 PM
	 * \param sub Given \a SubVectorCell object.
	 * \param index Given index of a \a SubVectorCell element.
	 * \return Value of \a SubVectorCell element of a given index.
	 */
	static Cell SubVectorCell_get(const SubVectorCell sub, const size_t index);

	/**
	 * \fn static void SubVectorCell_set(const SubVectorCell sub, const size_t index, const Cell value)
	 * \brief Sets value of \a SubVectorCell element of a given index.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:07 PM
	 * \param sub Given \a SubVectorCell object.
	 * \param index Given index of a \a SubVectorCell element.
	 * \param value Given value to which a \a SubVectorCell element of given index will be set.
	 */
	static void SubVectorCell_set(const SubVectorCell sub, const size_t index, const Cell value);

	/**
	 * \fn static SubVectorCell SubSubVectorCell_extract(const size_t start, const ssize_t step, const size_t end, const SubVectorCell sub)
	 * \brief Extracts a \a SubVectorCell object from another \a SubVectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:07 PM
	 * \param start Index corresponding to the \a SubVectorCell first element.
	 * \param step Offset between \a SubVectorCell elements (relative to a given \a SubVectorCell object).
	 * \param end Index corresponding to the \a SubVectorCell last element.
	 * \param sub Given \a SubVectorCell object on which to extract another \a SubVectorCell object. 
	 * \return Extracted \a SubVectorCell object.
	 */
	static SubVectorCell SubSubVectorCell_extract(const size_t start, const ssize_t step, const size_t end, const SubVectorCell sub);

	/**
	 * \fn static CursorCell CursorCell_init(const SubVectorCell sub)
	 * \brief Initializes and returns a \a CursorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:07 PM
	 * \param sub \a SubVectorCell object on which the cursor will be initialized.
	 * \return Initialized \a SubVectorCell object.
	 */
	static CursorCell CursorCell_init(const SubVectorCell sub);

	/**
	 * \fn static CursorCell CursorCell_next(const CursorCell cursor)
	 * \brief Displaces a \a CursorCell object onto the next element.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:07 PM
	 * \param cursor Given \a CursorCell object to displace.
	 * \return Displaced \a CursorCell object.
	 */
	static CursorCell CursorCell_next(const CursorCell cursor);

	/**
	 * \fn static Cell CursorCell_get(const CursorCell cursor)
	 * \brief Gets the value of the element of index stored in a given \a CursorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param cursor Given \a CursorCell object.
	 * \return Value of the element of index stored in a given \a CursorCell object.
	 */
	static Cell CursorCell_get(const CursorCell cursor);

	/**
	 * \fn static Cell CursorCell_at(const CursorCell cursor)
	 * \brief Gets the address of the element of index stored in a given \a CursorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param cursor Given \a CursorCell object.
	 * \return Address of the element of index stored in a given \a CursorCell object.
	 */
	static Cell * CursorCell_at(const CursorCell cursor);
	
	/**
	 * \fn static void CursorCell_set(const CursorCell cursor, const Cell value)
	 * \brief Sets element of index stored in a given \a CursorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param cursor Given \a CursorCell object.
	 * \param value A given \a Cell value.
	 */
	static void CursorCell_set(const CursorCell cursor, const Cell value);

	/**
	 * \fn static bool CursorCell_is_atend(const CursorCell cursor)
	 * \brief Tests if a given \a CursorCell object is at the end of its \a SubVectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param cursor Given \a CursorCell object.
	 * \return true if at the end else false.
	 */
	static bool CursorCell_is_atend(const CursorCell cursor);

	/**
	 * \fn static CursorCell CursorCell_next_none_zero(CursorCell cursor)
	 * \brief Displaces a given \CursorCell object to next non-null element of its \a SubVectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param cursor Given \a CursorCell object to displace.
	 * \return Displaced \a CursorCell object.
	 */
	static CursorCell CursorCell_next_none_zero(CursorCell cursor);

	/**
	 * \fn static void StackCursorCell_push_cursor(StackCursorCell * const stack, const CursorCell cursor0, const CursorCell cursor1)
	 * \brief Pushes \a CursorCell object only if they are not pointing to a null value and not at the end of their \a SubVectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param cursor0 Given \a CursorCell object.
	 * \param cursor1 Given \a CursorCell object.
	 */
	static void StackCursorCell_push_cursor(StackCursorCell * const stack, const CursorCell cursor0, const CursorCell cursor1);

	/**
	 * \fn static bool SubVectorCell_group(SubVectorCell sub, Cell * previous, StackCursorCell * const stack, unsigned int * const points)
	 * \brief Adds powers of 2 together only if they are equal, pushed non-null value to a given stack, and adds new earned points do the current number of points.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param sub \a SubVectorCell object.
	 * \param previous Address at which previous cursor1 pointed. (always NULL at call)
	 * \param stack Given empty stack.
	 * \param points Address to the current number of points.
	 * \return true if a 2048 value was found else false.
	 */
	static bool SubVectorCell_group(SubVectorCell sub, Cell * previous, StackCursorCell * const stack, unsigned int * const points);

	/**
	 * \fn static void SubVectorCell_shift(SubVectorCell sub, StackCursorCell * const stack)
	 * \brief Shifts all values at the end of a given \a SubVectorCell objects. Positions of the value to shift are stored a given stack.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param sub \a SubVectorCell object.
	 * \param stack Given stack.
	 */
	static void SubVectorCell_shift(SubVectorCell sub, StackCursorCell * const stack);

	/**
	 * \fn static bool SubVectorCell_swipe(SubVectorCell sub, StackCursorCell * stack, unsigned int * const points)
	 * \brief Swipes values of a \a SubVectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param param desc
	 * \return value desc
	 * Swiping in the context of this program, means to shift values of a vector, after having those equal combined.
	 */
	static bool SubVectorCell_swipe(SubVectorCell sub, StackCursorCell * stack, unsigned int * const points);

	/**
	 * \fn static bool periodic_binary_signal(const size_t n, const size_t ht)
	 * \brief Generates a sequence of binary value of period 2 * ht.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param n Abscissa coordinate.
	 * \param ht Half period.
	 * \return Either 1 or 0 depending on the abscissa \a n and half period \a ht.
	 */
	static bool periodic_binary_signal(const size_t n, const size_t ht);

	/**
	 * \fn static SubVectorCell SubVectorCell_extract_before_swiping(const CoreboardAction action, const size_t i, const size_t length, VectorCell * const board)
	 * \brief Extracts the correct \a SubVectorCell object depending on a given \a CoreboardAction user-level action and index.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param action Given \a CoreboardAction user-level action.
	 * \param i Given line or column index.
	 * \param length Side length of the \a VectorCell square area.
	 * \param board Given \a VectorCell object on which to extract the \a SubVectorCell object.
	 * \return Extracted \a SubVectorCell object.
	 */
	static SubVectorCell SubVectorCell_extract_before_swiping(const CoreboardAction action, const size_t i, const size_t length, VectorCell * const board);

	/**
	 * \fn bool VectorCell_swipe(VectorCell * const board, const CoreboardAction action, unsigned int * const points)
	 * \brief Swipes value of a given \a VectorCell object in the direction corresponding to a given \a CoreboardAction user-level action.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param board Given \a VectorCell object.
	 * \param action Given \a CoreboardAction user-level action.
	 * \param points Address to the current number of points.
	 * \return true if player won else false.
	 * This function also calculate the number of points made during the swipe and determine if the player won the game.
	 */
	bool VectorCell_swipe(VectorCell * const board, const CoreboardAction action, unsigned int * const points);

	/**
	 * \fn static ssize_t draw_index(const ssize_t min, const ssize_t max)
	 * \brief Returns random value in betwen min and max boundaries.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param min Minimum.
	 * \param max Maximum.
	 * \return Random value in between min and max boundaries.
	 */
	static ssize_t draw_index(const ssize_t min, const ssize_t max);

	/**
	 * \fn void VectorCell_insert_randomly(VectorCell * const board)
	 * \brief Sets a random \a Cell value to 2 or 4 in a give \a VectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param board Given \a VectorCell object.
	 */
	void VectorCell_insert_randomly(VectorCell * const board);

	/**
	 * \fn static bool SubVectorCell_is_subset_movable(const SubVectorCell sub[2])
	 * \brief Tests if motion is possible in any 4 directions (up, left, down, right) in a given sub portion of a \a VectorCell object.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:08 PM
	 * \param sub Sub portion of a \a VectorCell object.
	 * \return true if motion is possible else false.
	 */
	static bool SubVectorCell_is_subset_movable(const SubVectorCell sub[2]);

	/**
	 * \fn bool VectorCell_has_lost(VectorCell * const board)
	 * \brief Tests player has lost the game. That is to say if no more motion is possible anywhere inside a given \a
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:09 PM
	 * \param board Given \a VectorCell object.
	 * \return true if at least 1 motion possibility is detected.
	 * Motion possibility detection is achieved by dividing a given \a VectorCell object into squares of 2 * 2 cells,
	 * then testing motion possibility inside those squares until at least 1 square contains at least 1 motion possibility.
	 */
	bool VectorCell_has_lost(VectorCell * const board);

	/**
	 * \fn static bool SubVectorCell_is_action_valid(SubVectorCell sub)
	 * \brief Tests all non null values of a \a SubVectorCell object are not already at its end.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:09 PM
	 * \param 
	 * \return true if values are already at its end else false.
	 */
	static bool SubVectorCell_is_action_valid(SubVectorCell sub);

	/**
	 * \fn bool VectorCell_is_action_valid(VectorCell * const board, const CoreboardAction action)
	 * \brief Tests if a given \a CoreboardAction user-level action is valid.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-13 Sat 03:09 PM
	 * \param board Given \a VectorCell object.
	 * \param action Given \a CoreboardAction user-level action.
	 * \return true is action is valid else false
	 * Action validity is determined by checking if applying motion in a given direction will change at least 1 value in a given \a VectorCell object.
	 */
	bool VectorCell_is_action_valid(VectorCell * const board, const CoreboardAction action);

#endif
