/**
 * \file save.h
 * \brief Headers related to routines used for save functionalities 
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef SAVE_H
	#define SAVE_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <sys/stat.h>
	#include <sys/types.h>
	#include <string.h>
	#include <time.h>

	#include "coreboard.h"
	#include "terminal_io.h"
	#include "stat.h"
	#include "../Lib/Vector/vector.h"

	/**
	 * \struct Game
	 * \date 2020-06-14 Sun 12:25 AM
	 * \brief This structure represents game related data structure.
	 * \var coreBoard
	 * \a VectorCell board.
	 * \var stat
	 * Statistics related to the current game.
	 */
	struct Game
	{
		VectorCell * coreBoard;
		Stat stat;
	};

	typedef struct Game Game;

	/**
	 * \fn static void mkdir_data(void)
	 * \brief makes the Data directory is it does not exist.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:07 AM
	 */
	static void mkdir_data(void);

	/**
	 * \fn static char * get_valid_save_name(char * msg)
	 * \brief Gets a non empty string.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:08 AM
	 * \param msg Message to prompt to the user.
	 * \return Valid save name.
	 */
	static char * get_valid_save_name(char * msg);

	/**
	 * \fn static void fwrite_game(const VectorCell * const coreBoard, const Stat stat, FILE * const save)
	 * \brief Writes the current board state and current stats in a save file.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:09 AM
	 * \param coreBoard \a VectorCell object.
	 * \param stat Current Stats.
	 * \param save Save file.
	 */
	static void fwrite_game(const VectorCell * const coreBoard, const Stat stat, FILE * const save);

	/**
	 * \fn void Game_save(const Game game)
	 * \brief saves the current board state (Calls \a fwrite_game).
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:09 AM
	 * \param game Current game state.
	 */
	void Game_save(const Game game);

	/**
	 * \fn char * get_valid_load_name(void)
	 * \brief Gets a valid load path or skips
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:09 AM
	 * \return Valid load path or "Data/" string if user pressed enter.
	 */
	char * get_valid_load_name(void);

	/**
	 * \fn static Game fread_game(FILE * const load)
	 * \brief Reads saved data from save file.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:09 AM
	 * \param load Load file.
	 * \return Data retrieved from load file.
	 */
	static Game fread_game(FILE * const load);

	/**
	 * \fn Game Game_load(void)
	 * \brief Loads previously saved data (Calls \a fread_game).
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:09 AM
	 * \return Data retrieved from load file.
	 */
	Game Game_load(void);

	/**
	 * \fn void Stat_save(const Stat stat)
	 * \brief Saves statistics at the end of the game in historic file.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:09 AM
	 * \param stat End game stats.
	 */
	void Stat_save(const Stat stat);

	/**
	 * \fn void Stat_load(void)
	 * \brief Displays statistics historic.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 05:09 AM
	 */
	void Stat_load(void);
#endif
