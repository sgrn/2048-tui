/**
 * \file terminal_io.c
 * \brief Contains routines specific to Input / Output terminal.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-04 Mon 07:39 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "terminal_io.h"

int secure_inputint(char * inputMsg)
{
	char * buff;
	size_t size;
	ssize_t ret, i;
	int result;
	buff = NULL;
	size = 0;
	do {
		printf(inputMsg);
		ret = getline(&buff, &size, stdin);
		if (ret == -1) {
			fprintf(stderr, "error: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		i = 0;
		while ((buff[i] >= 48) && (buff[i] <= 57)) {
			i++;
		}
	} while (i != (ret - 1));
	sscanf(buff, "%d", &result);
	free(buff);
	return result;
}

char * secure_inputstr(char * inputMsg)
{
	char * string;
	size_t n;
	ssize_t ret;
	string = NULL;
	n = 0;
	printf(inputMsg);
	ret = getline(&string, &n, stdin);
	if (ret == -1) {
		fprintf(stderr, "error: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	string[ret - 1] = '\0';
	return string;
}

void clear_screen(void) {
	printf("\033[2J\033[0;0H");
}
