/**
 * \file stat.c
 * \brief Routines used for statistics calculation.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "stat.h"

void Stat_print(const time_t t, const Stat stat)
{
	struct tm tm;
	tm = *localtime(&t);
	puts("---------------------------");
	printf("DATE : %d-%02d-%02d %02d:%02d:%02d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	printf("SCORE : %u\n", stat.score);
	printf("AVERAGE POINTS PER MOVE : %0.1f\n", stat.averagePointsPerMove);
	printf("TOTAL NUMBER OF MOVES : %u\n", stat.motionCounter);
	printf("NUMBER OF RIGHT MOVES : %u\n", stat.directedMotionCounter[COREBOARD_RIGHT]);
	printf("NUMBER OF LEFT MOVES : %u\n", stat.directedMotionCounter[COREBOARD_LEFT]);
	printf("NUMBER OF DOWN MOVES : %u\n", stat.directedMotionCounter[COREBOARD_DOWN]);
	printf("NUMBER OF UP MOVES : %u\n", stat.directedMotionCounter[COREBOARD_UP]);
	printf("TOTAL TIME : %lu seconds\n", stat.totalTime);
	printf("TIME BETWEEN MOVES : %0.3lf seconds\n", stat.averageTimeBetweenMoves);
	puts("---------------------------");
}

double Stat_get_time(void)
{
	struct timespec t;
	if (clock_gettime(CLOCK_REALTIME, &t) == -1) {
		fprintf(stderr, "error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return t.tv_sec + t.tv_nsec * 1E-9;
}

void Stat_refresh(Stat * const stat, const double diffTime, const unsigned short diffPts, CoreboardAction action)
{
	stat->averageTimeBetweenMoves = (stat->motionCounter * stat->averageTimeBetweenMoves + diffTime) / (stat->motionCounter + 1);
	stat->averagePointsPerMove = (stat->motionCounter * stat->averagePointsPerMove + diffPts) / (++stat->motionCounter);
	stat->directedMotionCounter[action]++;
}

Stat Stat_init(void)
{
	Stat stat;
	stat.score = 0;
	stat.averagePointsPerMove = 0;
	stat.motionCounter = 0;
	stat.directedMotionCounter[COREBOARD_RIGHT] = 0;
	stat.directedMotionCounter[COREBOARD_LEFT] = 0;
	stat.directedMotionCounter[COREBOARD_DOWN] = 0;
	stat.directedMotionCounter[COREBOARD_UP] = 0;
	stat.totalTime = time(NULL);
	stat.averageTimeBetweenMoves = 0;
	return stat;
}
