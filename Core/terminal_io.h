/**
 * \file terminal_io.h
 * \brief Contains headers of routines specific to Input / Output terminal.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-05-04 Mon 07:39 AM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __TERMINAL_IO__
	#define __TERMINAL_IO__
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <errno.h>
	
	/**
	 * \fn int secure_inputint(char * inputMsg)
	 * \brief Returns integer from stdin securely.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 07:36 AM
	 * \param inputMsg Message to output.
	 * \return integer from stdin.
	 */
	int secure_inputint(char * inputMsg);

	/**
	 * \fn char * secure_inputstr(char * inputMsg)
	 * \brief Returns as string from stdin securely.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 07:36 AM
	 * \param inputMsg Message to output.
	 * \return string from stdin.
	 */
	char * secure_inputstr(char * inputMsg);

	/**
	 * \fn void clear_screen(void)
	 * \brief clears terminal screen. 
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-05-03 Mon 07:36 AM
	 */
	void clear_screen(void);

#endif
