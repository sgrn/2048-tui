/**
 * \file game.c
 * \brief Routines used for game data structures initialization.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "game.h"

int Game_get_board_side_length(void)
{
	int option;
	puts("Choose board dimensions: ");
	puts("0\t- 4 x 4 (Hardcore)");
	puts("1\t- 5 x 5 (Difficult)");
	puts("2\t- 6 x 6 (Normal)");
	puts("3\t- 7 x 7 (Simple)");
	puts("4\t- 8 x 8 (Easy)");
	do {
		option = secure_inputint("Enter board option ? [0 - 4] : ");
	} while (option > 4);
	return 4 + option;
}

Game Game_start(void)
{
	Game game;
	clear_screen();
	game = Game_load();
	if (game.coreBoard == NULL) {
		game.coreBoard = VectorCell_allocinit(Game_get_board_side_length());
		VectorCell_insert_randomly(game.coreBoard);
		VectorCell_insert_randomly(game.coreBoard);
		game.stat = Stat_init();
	}
	return game;
}

VECTOR_FREE(Cell, Cell)

void Game_end(Game * const game)
{
	VectorCell_free(&game->coreBoard);
	game->stat.totalTime = time(NULL) - game->stat.totalTime;
	Stat_save(game->stat);
}
