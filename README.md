# Ncurses version of the 2048 GAME.

![2048 game](Img/board.png)

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

## About

This project is an ncurses TUI version of the infamous 2048 puzzle game. It is payable using the arrow keys and the program outputs a colored interactive board.
The interface is responsive, therefore the board always stays centered whenever the terminal window is being re-sized. Though the interface adaptability should work great on any ECMA-48 compliant terminal with at least 3/4 bit color, I only tested it on the following terminals :

* xterm-256color 
* xterm
* rxvt-unicode-256color

I have not had the opportunity to explore the extent of it on other terminals, nevertheless, I cannot see why it would not work elsewhere as well.
Additionally, the game allows you to *create and load saves, display detailed statistics of all previous games, such as player response time, scores, or even number of moves
, to only state a few*. 

Once inside a game you can always display the following help information by pressing the 'h' key :

![Help menu](Img/help_menu.png)

## How to compile and execute on GNU/Linux.

NCURSES is a dependency of this project. [Here](https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/intro.html#WHERETOGETIT) are instructions on how to install it.

Compile.

`make`

Execute.

`./2048`
