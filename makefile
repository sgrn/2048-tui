compiler = gcc
objects = main.o stack.o vector.o coreboard.o game.o save.o terminal_io.o \
stat.o tui_board.o interface.o

2048 : $(objects)
	$(compiler) -o 2048 $(objects) -lncurses -lm

main.o : main.c
	$(compiler) -c main.c

coreboard.o : Core/coreboard.c Core/coreboard.h
	$(compiler) -c Core/coreboard.c

game.o : Core/game.c Core/game.h
	$(compiler) -c Core/game.c

save.o : Core/save.c Core/save.h
	$(compiler) -c Core/save.c

terminal_io.o : Core/terminal_io.c Core/terminal_io.h
	$(compiler) -c Core/terminal_io.c

stat.o : Core/stat.c Core/stat.h
	$(compiler) -c Core/stat.c

tui_board.o : UI/tui_board.c UI/tui_board.h
	$(compiler) -c UI/tui_board.c

interface.o : UI/interface.c UI/interface.h
	$(compiler) -c UI/interface.c

vector.o : Lib/Vector/vector.h Lib/Vector/vector.c
	$(compiler) -c Lib/Vector/vector.c

stack.o : Lib/Stack/stack.h Lib/Stack/stack.c
	$(compiler) -c Lib/Stack/stack.c

.PHONY : clean
clean :
	rm 2048 $(objects)
