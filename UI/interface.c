/**
 * \file interface.c
 * \brief Routines used for interfacing with the user.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "interface.h"

static Info TUI_start(void)
{
	Info info;
	size_t squareRoot;
	int nLines, nCols;
	Game game;
	BoardWindow * board;
	game = Game_start();
	initscr();
	raw();
	noecho();
	curs_set(0);
	keypad(stdscr, TRUE);
	if (! has_colors()) {
		endwin();
		fprintf(stderr, "Your terminal doesn't have any colors, yet colors are requirements to play this game.\n");
		exit(EXIT_FAILURE);
	}
	start_color();
	clear();
	squareRoot = sqrtf(game.coreBoard->capacity);
	nLines = squareRoot * SUBWIN_HEIGHT + 4;
	nCols = squareRoot * SUBWIN_WIDTH + 2;
	lock_screen_till_at_least(nLines + 4, nCols + 4);
	board = BoardWindow_allocinit(game);
	info.game = game;
	info.board = board;
	info.state = true;
	return info;
}

static void TUI_stop(Info * const info)
{
	Game_end(&info->game);
	BoardWindow_free(&info->board);
	info->state = false;
	endwin();
}

static Info TUI_restart(Info * const info)
{
	TUI_stop(info);
	return TUI_start();
}

static void TUI_save(Info * const info)
{
	def_prog_mode();
	endwin();
	clear_screen();
	Game_save(info->game);
	reset_prog_mode();
	if (refresh() == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
	BoardWindow_adapt_if_rezised(&info->board, info->game);
}

static void TUI_stat(Info * const info)
{
	def_prog_mode();
	endwin();
	clear_screen();
	Stat_load();
	puts("\npress enter to get back at the game...");
	getchar();
	reset_prog_mode();
	if (refresh() == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
	BoardWindow_adapt_if_rezised(&info->board, info->game);
}

static void TUI_help(Info * const info)
{
	def_prog_mode();
	endwin();
	clear_screen();
	puts("MENU OPTIONS:\n");
	puts("(KEYS)\t(PURPOSE)");
	puts("q\tQuits the game.");
	puts("h\tDisplays help menu.");
	puts("s\tSaves the current game.");
	puts("t\tDisplays statistics of previous game(s).");
	puts("r\tRestarts the game.");
	puts("\nMOTIONS:\n");
	puts("(KEYS)\t(PURPOSE)");
	puts("UP ARROW\tMoves all cells upwards");
	puts("DOWN ARROW\tMoves all cells downwards");
	puts("RIGHT ARROW\tMoves all cells leftwards.");
	puts("LEFT ARROW\tMoves all cells rightwards.");
	puts("\npress enter to get back at the game...");
	getchar();
	reset_prog_mode();
	if (refresh() == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
	BoardWindow_adapt_if_rezised(&info->board, info->game);
}

static CoreboardAction TUI_catch_event(Info * const info)
{
	CoreboardAction action;
	int keyevent;
	keyevent = getch();
	action = COREBOARD_NONE;
	switch (keyevent) {
		case KEY_LEFT:
			action = COREBOARD_LEFT;
			break;
		case KEY_RIGHT:
			action = COREBOARD_RIGHT;
			break;
		case KEY_UP:
			action = COREBOARD_UP;
			break;
		case KEY_DOWN:
			action = COREBOARD_DOWN;
			break;
		case 'q':
			TUI_stop(info);
			break;
		case 'h':
			TUI_help(info);
			break;
		case 's':
			TUI_save(info);
			break;
		case 't':
			TUI_stat(info);
			break;
		case 'r':
			*info = TUI_restart(info);
			break;
		case KEY_RESIZE:
			BoardWindow_adapt_if_rezised(&info->board, info->game);
			break;
		default:
			break;
	}
	return action;
}

static void TUI_act(Info * const info, const CoreboardAction action)
{
	int previousScore;
	if (VectorCell_is_action_valid(info->game.coreBoard, action)) {
		previousScore = info->game.stat.score;
		info->hasWon = VectorCell_swipe(info->game.coreBoard, action, &info->game.stat.score);
		VectorCell_insert_randomly(info->game.coreBoard);
		info->hasLost = VectorCell_has_lost(info->game.coreBoard);
		info->scoreDiff = info->game.stat.score - previousScore;
		Stat_refresh(&info->game.stat, info->responseTime, info->scoreDiff, action);
		StatusBar_sync(info->board, info->game.stat);
		BoardWindow_sync(info->board, info->game.coreBoard);
		if (info->hasWon || info->hasLost) {
			if (StatusBar_display_endgame_prompt(info->board, info->hasWon)) {
				*info = TUI_restart(info);
			} else {
				TUI_stop(info);
			}
		}
	}
}

void TUI_run(void)
{
	Info info;
	CoreboardAction action;
	info = TUI_start();
	do {
		info.responseTime = Stat_get_time();
		action = TUI_catch_event(&info);
		info.responseTime = Stat_get_time() - info.responseTime;
		if (action != COREBOARD_NONE) {
			TUI_act(&info, action);
		}
	} while (info.state);
	printf("\nGood bye ! :).\n");
}
