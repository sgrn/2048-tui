/**
 * \file tui_board.h
 * \brief Headers related to routines used for managing the TUI.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef TUI_BOARD_H
	#define TUI_BOARD_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <ncurses.h>
	#include <math.h>

	#include "../Lib/Vector/vector.h"
	#include "../Core/coreboard.h"
	#include "../Core/game.h"
	#include "../Core/stat.h"

	#define SUBWIN_WIDTH 6
	#define SUBWIN_HEIGHT 3

	VECTOR(WINDOW *, WINDOW)

	/**
	 * \struct BoardWindow
	 * \date 2020-06-14 Sun 12:25 AM
	 * \brief This structure represents TUI related data.
	 * \var win
	 * Main window containing the bar and the board.
	 * \var subwins
	 * Sets of windows composing the board.
	 * \var originalScreenHeight
	 * Screen height during window allocation.
	 * \var originalScreenWidth
	 * Screen width during window allocation.
	 * \var nLines
	 * Current number of lines taken by the main window.
	 * \var nCols
	 * Current number of columns taken by the main window.
	 */
	struct BoardWindow 
	{
		WINDOW * win;
		VectorWINDOW * subwins;
		int originalScreenHeight;
		int originalScreenWidth;
		int nLines;
		int nCols;
	};

	typedef struct BoardWindow BoardWindow;

	/**
	 * \fn static VectorWINDOW * VectorWINDOW_allocinit(WINDOW * const win, const size_t squareRoot)
	 * \brief Allocates and initializes a \a VectorWINDOW object of sub windows composing the board.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param win Main window.
	 * \param squareRoot side length (in sub window).
	 * \return Newly allocated and initialized \VectorWINDOW object.
	 */
	static VectorWINDOW * VectorWINDOW_allocinit(WINDOW * const win, const size_t squareRoot);

	/**
	 * \fn static BoardWindow * BoardWindow_allocate(const size_t squareRoot)
	 * \brief Allocates the TUI.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param squareRoot side length (in sub window).
	 * \return Newly allocated \a BoardWindow.
	 */
	static BoardWindow * BoardWindow_allocate(const size_t squareRoot);

	/**
	 * \fn static void BoardWindow_init_pairs(void)
	 * \brief Initializes color pairs.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 */
	static void BoardWindow_init_pairs(void);

	/**
	 * \fn static void BoardWindow_set(BoardWindow * const board, const size_t i, const size_t j, const Cell value)
	 * \brief Sets state of a sub window of a given \a i, \a j indexes.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param board Given \a BoardWindow object.
	 * \param i Given line index.
	 * \param j Given column index.
	 * \param value Given \a Cell value.
	 */
	static void BoardWindow_set(BoardWindow * const board, const size_t i, const size_t j, const Cell value);

	/**
	 * \fn static Cell BoardWindow_get(BoardWindow * const board, const size_t i, const size_t j)
	 * \brief Gets state of a sub window of given \a i, \a j indexes.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param board Given \a BoardWindow object.
	 * \param i Given line index.
	 * \param j Given column index.
	 * \return Cell value of a given sub window.
	 */
	static Cell BoardWindow_get(BoardWindow * const board, const size_t i, const size_t j);

	/**
	 * \fn void BoardWindow_sync(BoardWindow * const board, const VectorCell * const coreBoard)
	 * \brief Syncs TUI with low-level game data structures.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param board Given \a BoardWindow object.
	 * \param coreBoard Given \a VectorCell object.
	 */
	void BoardWindow_sync(BoardWindow * const board, const VectorCell * const coreBoard);

	/**
	 * \fn bool StatusBar_display_endgame_prompt(BoardWindow * const board, const bool wonOrLost)
	 * \brief Displays end game prompt, asking user to either restart or quit the game.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param board Given \a BoardWindow object.
	 * \param wonOrLost Winning or Loosing state.
	 * \return true if the user chose to restart else false.
	 */
	bool StatusBar_display_endgame_prompt(BoardWindow * const board, const bool wonOrLost);

	/**
	 * \fn void StatusBar_sync(BoardWindow * const board, const Stat stat)
	 * \brief Syncs bar with current stats.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param board Given \a BoardWindow object.
	 * \param stat Given \a Stats object.
	 */
	void StatusBar_sync(BoardWindow * const board, const Stat stat);

	/**
	 * \fn static void StatusBar_init(WINDOW * const bar, int width)
	 * \brief Initializes Status bar.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param bar Given status bar window.
	 * \param width screen-relative bar width.
	 */
	static void StatusBar_init(WINDOW * const bar, int width);

	/**
	 * \fn static void BoardWindow_init(BoardWindow * const board, const Game game, const size_t squareRoot)
	 * \brief Initializes TUI.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:57 AM
	 * \param board Given \a BoardWindow object.
	 * \param game Given \a Game object.
	 * \param squareRoot side length (in sub window);
	 */
	static void BoardWindow_init(BoardWindow * const board, const Game game, const size_t squareRoot);

	/**
	 * \fn BoardWindow * BoardWindow_allocinit(const Game game)
	 * \brief Allocates and initializes TUI.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:58 AM
	 * \param game Given \a Game object.
	 * \return Newly allocated \a BoardWindow.
	 */
	BoardWindow * BoardWindow_allocinit(const Game game);

	/**
	 * \fn bool lock_screen_till_at_least(const int leastHeight, const int leastWidth)
	 * \brief Locks the screen till its dimensions are at least \a leastHeight and \a leastWidth.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:58 AM
	 * \param leastHeight Least possible height.
	 * \param leastWidth Least possible width.
	 * \return true if the screen has been locked else false.
	 */
	bool lock_screen_till_at_least(const int leastHeight, const int leastWidth);

	/**
	 * \fn void BoardWindow_adapt_if_rezised(BoardWindow * * const board, const Game game)
	 * \brief Adapts the Text board position if the screen dimensions changed.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:58 AM
	 * \param board Address if a \a BoardWindow object.
	 * \param game Given \a Game object.
	 */
	void BoardWindow_adapt_if_rezised(BoardWindow * * const board, const Game game);

	/**
	 * \fn void BoardWindow_free(BoardWindow * * const board)
	 * \brief Frees the TUI.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:58 AM
	 * \param board Address if a \a BoardWindow object.
	 */
	void BoardWindow_free(BoardWindow * * const board);
#endif
