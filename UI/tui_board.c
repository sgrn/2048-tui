/**
 * \file tui_board.c
 * \brief Routines used for managing the TUI.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "tui_board.h"

VECTOR_MALLOC(WINDOW *, WINDOW)

VECTOR_SET(WINDOW *, WINDOW)

static VectorWINDOW * VectorWINDOW_allocinit(WINDOW * const win, const size_t squareRoot)
{
	VectorWINDOW * subwins;
	size_t i, j;
	int y, x;
	WINDOW * subwin;
	subwins = VectorWINDOW_malloc(squareRoot * squareRoot);
	for (i = 0; i < squareRoot; i++) {
		for (j = 0; j < squareRoot; j++) {
			y = 3 + i * SUBWIN_HEIGHT;
			x = 1 + j * SUBWIN_WIDTH;
			if ((subwin = derwin(win, SUBWIN_HEIGHT, SUBWIN_WIDTH, y, x)) == NULL) {
				endwin();
				fprintf(stderr, "error : cannot allocate ncurses sub window.\n");
				exit(EXIT_FAILURE);
			}
			VectorWINDOW_set(subwins, i * squareRoot + j, subwin);
		}
	}
	return subwins;
}

static BoardWindow * BoardWindow_allocate(const size_t squareRoot)
{
	BoardWindow * board;
	int maxY, maxX, nLines, nCols, y, x;
	if ((board = malloc(sizeof(BoardWindow))) == NULL) {
		endwin();
		fprintf(stderr, "error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	getmaxyx(stdscr, maxY, maxX);
	board->originalScreenHeight = maxY;
	board->originalScreenWidth = maxX;
	nLines = squareRoot * SUBWIN_HEIGHT + 4;
	nCols = squareRoot * SUBWIN_WIDTH + 2;
	board->nLines = nLines;
	board->nCols = nCols;
	y = (maxY - nLines) / 2;
	x = (maxX - nCols) / 2;
	if ((board->win = newwin(nLines, nCols, y, x)) == NULL) {
		endwin();
		fprintf(stderr, "error : cannot allocate ncurses window.\n");
		exit(EXIT_FAILURE);
	}
	board->subwins = VectorWINDOW_allocinit(board->win, squareRoot);
	return board;
}

static void BoardWindow_init_pairs(void)
{
	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_BLACK, COLOR_WHITE);
	init_pair(3, COLOR_BLACK, COLOR_YELLOW);
	init_pair(4, COLOR_WHITE, COLOR_GREEN);
	init_pair(5, COLOR_BLACK, COLOR_GREEN);
	init_pair(6, COLOR_WHITE, COLOR_CYAN);
	init_pair(7, COLOR_BLACK, COLOR_CYAN);
	init_pair(8, COLOR_WHITE, COLOR_BLUE);
	init_pair(9, COLOR_BLACK, COLOR_BLUE);
	init_pair(10, COLOR_WHITE, COLOR_MAGENTA);
	init_pair(11, COLOR_BLACK, COLOR_MAGENTA);
	init_pair(12, COLOR_BLACK, COLOR_RED);
}

VECTOR_GET(WINDOW *, WINDOW)

static void BoardWindow_set(BoardWindow * const board, const size_t i, const size_t j, const Cell value)
{
	size_t squareRoot, k;
	WINDOW * subwin;
	int power, pair, nDigits;
	bool isNotNull;
	squareRoot = sqrtf(board->subwins->capacity);
	subwin = VectorWINDOW_get(board->subwins, i * squareRoot + j);
	power = logf(value) / logf(2);
	isNotNull = value != 0;
	pair = isNotNull * power + 1;
	wattron(subwin, COLOR_PAIR(pair));
	for (k = 0; k < SUBWIN_HEIGHT; k++) {
		mvwhline(subwin, k, 0, ' ', SUBWIN_WIDTH);
	}
	if (isNotNull) {
		power = logf(value) / logf(10);
		nDigits = isNotNull * (power + 1);
		mvwprintw(subwin, 1, (SUBWIN_WIDTH - nDigits) / 2, "%u", value);
	}
	wattroff(subwin, COLOR_PAIR(pair));
	if ((touchwin(board->win) == ERR) || (wrefresh(subwin) == ERR)) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

static Cell BoardWindow_get(BoardWindow * const board, const size_t i, const size_t j)
{
	size_t squareRoot;
	WINDOW * subwin;
	char line[SUBWIN_WIDTH + 1];
	Cell value;
	squareRoot = sqrtf(board->subwins->capacity);
	subwin = VectorWINDOW_get(board->subwins, i * squareRoot + j);
	if (mvwinnstr(subwin, 1, 0, line, SUBWIN_WIDTH) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot extract line from curses.\n");
		exit(EXIT_FAILURE);
	}
	value = 0;
	sscanf(line, "%u", &value);
	return value;
}

VECTOR_GET(Cell, Cell)

void BoardWindow_sync(BoardWindow * const board, const VectorCell * const coreBoard)
{
	size_t squareRoot, i, j;
	Cell boardCell, coreBoardCell;
	squareRoot = sqrtf(coreBoard->capacity);
	for (i = 0; i < squareRoot; i++) {
		for (j = 0; j < squareRoot; j++) {
			coreBoardCell = VectorCell_get(coreBoard, i * squareRoot + j);
			boardCell = BoardWindow_get(board, i, j);
			if (coreBoardCell != boardCell) {
				BoardWindow_set(board, i, j, coreBoardCell);
			}
		}
	}
}

bool StatusBar_display_endgame_prompt(BoardWindow * const board, const bool wonOrLost)
{
	static char * options[2] = {"LOST", "WON"};
	bool choice;
	WINDOW * bar;
	int ch;
	bar = board->win;
	mvwprintw(bar, 1, 0, "You %s, restart ? Y/n: ", options[wonOrLost]);
	if (wrefresh(bar) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
	do {
		ch = wgetch(bar);
		choice = (ch == 'Y') || (ch == 'y');
	} while ((! choice) && (ch != 'N') && (ch != 'n'));
	return choice;
}

void StatusBar_sync(BoardWindow * const board, const Stat stat)
{
	WINDOW * bar;
	bar = board->win;
	wattron(bar, COLOR_PAIR(1));
	wmove(bar, 1, 5);
	whline(bar, ' ', 6);
	wprintw(bar, "%u", stat.score);
	wmove(bar, 1, 21);
	whline(bar, ' ', 4);
	wprintw(bar, "%0.1f", stat.averagePointsPerMove);
	wattroff(bar, COLOR_PAIR(1));
	if (wrefresh(bar) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
}

static void StatusBar_init(WINDOW * const bar, int width)
{
	static char * options[5] = {"Quit: q  ", "Help: h  ", "Save: s  ", "Stat: t  ", "Restart: r"};
	size_t i;
	int rest;
	wattron(bar, COLOR_PAIR(1));
	box(bar, 0, 0);
	mvwaddch(bar, 2, 0, ACS_ULCORNER);
	whline(bar, ACS_HLINE, width - 2);
	mvwaddch(bar, 2, width - 1, ACS_URCORNER);
	mvwaddstr(bar, 1, 0, "Tot: 0      Avg/Mov: 0");
	whline(bar, ' ', width - 22);
	mvwaddch(bar, 1, width - 1, ' ');
	wattroff(bar, COLOR_PAIR(1));
	wattron(bar, COLOR_PAIR(2));
	wmove(bar, 0, 0);
	rest = width;
	i = 0;
	while ((i < 5) && ((rest - (int)strlen(options[i])) > 0)) {
		waddstr(bar, options[i]);
		rest -= strlen(options[i]);
		i++;
	} 
	whline(bar, ' ', rest);
	wattroff(bar, COLOR_PAIR(2));
}

static void BoardWindow_init(BoardWindow * const board, const Game game, const size_t squareRoot)
{
	WINDOW * win;
	size_t internalWidth;
	win = board->win;
	internalWidth = squareRoot * SUBWIN_WIDTH;
	if (refresh() == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
	BoardWindow_init_pairs();
	StatusBar_init(win, internalWidth + 2);
	if (wrefresh(win) == ERR) {
		endwin();
		fprintf(stderr, "error ncurses : cannot refresh a window.\n");
		exit(EXIT_FAILURE);
	}
	BoardWindow_sync(board, game.coreBoard);
}

BoardWindow * BoardWindow_allocinit(const Game game)
{
	BoardWindow * board;
	size_t squareRoot;
	squareRoot = sqrtf(game.coreBoard->capacity);
	board = BoardWindow_allocate(squareRoot);
	BoardWindow_init(board, game, squareRoot);
	return board;
}

bool lock_screen_till_at_least(const int leastHeight, const int leastWidth)
{
	int currHeight, currWidth, midY, midX;
	bool hasBeenLocked;
	getmaxyx(stdscr, currHeight, currWidth);
	if (hasBeenLocked = ((currHeight < leastHeight) || (currWidth < leastWidth))) {
		clear();
		attron(A_BOLD);
		do { 
			midY = (currHeight - 1) / 2;
			midX = (currWidth - 73) / 2;
			move(midY, midX);
			printw("<--Not enough space. Enlarge the window. %dx%d-->", currHeight, currWidth);
			//move(midY, midX);
			if (refresh() == ERR) {
				endwin();
				fprintf(stderr, "error ncurses : cannot refresh a window.\n");
				exit(EXIT_FAILURE);
			}
			while (getch() != KEY_RESIZE);
			clear();
			//clrtoeol();
			getmaxyx(stdscr, currHeight, currWidth);
		} while ((currHeight < leastHeight) || (currWidth < leastWidth));
		attroff(A_BOLD);
	}
	return hasBeenLocked;
}

VECTOR_FREE(WINDOW *, WINDOW)

void BoardWindow_free(BoardWindow * * const board)
{
	BoardWindow * tmp;
	size_t squareRoot, i, j;
	WINDOW * subwin;
	tmp = *board;
	squareRoot = sqrtf(tmp->subwins->capacity);
	for (i = 0; i < squareRoot; i++) {
		for (j = 0; j < squareRoot; j++) {
			subwin = VectorWINDOW_get(tmp->subwins, i * squareRoot + j);
			if (delwin(subwin) == ERR) {
				endwin();
				fprintf(stderr, "error : cannot delete ncurses sub window.\n");
				exit(EXIT_FAILURE);
			}
		}
	}
	if (delwin(tmp->win) == ERR) {
		endwin();
		fprintf(stderr, "error : cannot delete ncurses sub window.\n");
		exit(EXIT_FAILURE);
	}
	free(tmp);
	*board = NULL;
}

void BoardWindow_adapt_if_rezised(BoardWindow * * const board, const Game game)
{
	BoardWindow * new;
	bool hasBeenLocked, hasChanged;
	int currHeight, currWidth, midY, midX;
	new = *board;
	hasBeenLocked = lock_screen_till_at_least(new->nLines + 4, new->nCols + 4);
	getmaxyx(stdscr, currHeight, currWidth);
	hasChanged = (new->originalScreenHeight != currHeight) || (new->originalScreenWidth != currWidth);
	if (hasChanged || hasBeenLocked) {
		clear();
		midY = (currHeight - new->nLines) / 2;
		midX = (currWidth - new->nCols) / 2;
		new->originalScreenHeight = currHeight;
		new->originalScreenWidth = currWidth;
		BoardWindow_free(board);
		*board = BoardWindow_allocinit(game);
		if ((refresh() == ERR) || (wrefresh(new->win) == ERR)) {
			endwin();
			fprintf(stderr, "error ncurses : cannot refresh a window.\n");
			exit(EXIT_FAILURE);
		}
	}
}
