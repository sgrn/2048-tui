/**
 * \file interface.h
 * \brief Headers related to routines used for interfacing with the user.
 * \author Sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-06-13 Sat 02:59 PM
 * Copyright (C) 2020 Sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef INTERFACE_H
	#define INTERFACE_H

	#include <ncurses.h>
	#include <time.h>
	#include <math.h>

	#include "tui_board.h"
	#include "../Core/coreboard.h"
	#include "../Core/game.h"
	#include "../Core/stat.h"
	#include "../Core/save.h"
	#include "../Core/terminal_io.h"


	/**
	 * \struct Info
	 * \date 2020-06-14 Sun 12:25 AM
	 * \brief This structure regroups both low-level and high-level game data structures.
	 * \var game
	 * \a Game object.
	 * \var board
	 * \a BoardWindow object.
	 * \var state
	 * Game activity state.
	 * \var hasLost
	 * Losing state.
	 * \var hasWon
	 * Winning state.
	 * \var responseTime
	 * Current player response time.
	 * \var scoreDiff
	 * Points recently earned.
	 */
	struct Info
	{
		Game game;
		BoardWindow * board;
		bool state;
		bool hasLost;
		bool hasWon;
		double responseTime;
		int scoreDiff;
	};

	typedef struct Info Info;

	/**
	 * \fn static Info TUI_start(void)
	 * \brief Initializes the game data structures and text interface.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 * \return Game low and high level information.
	 */
	static Info TUI_start(void);

	/**
	 * \fn static void TUI_stop(Info * const info)
	 * \brief Stops the game, frees data structure and text interface.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 * \param info Address of the game info to free.
	 */
	static void TUI_stop(Info * const info);

	/**
	 * \fn static Info TUI_restart(Info * const info)
	 * \brief Restarts the game.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 * \param info Address of the game info to free.
	 * \return Info of the newly restarted game.
	 */
	static Info TUI_restart(Info * const info);

	/**
	 * \fn static void TUI_save(Info * const info)
	 * \brief Saves the game.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 * \param info \a Info object.
	 */
	static void TUI_save(Info * const info);

	/**
	 * \fn static void TUI_stat(Info * const info)
	 * \brief Displays stats.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 * \param info \a Info object.
	 */
	static void TUI_stat(Info * const info);

	/**
	 * \fn static void TUI_help(Info * const info)
	 * \brief Displays help.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 * \param info \a Info object.
	 */
	static void TUI_help(Info * const info);

	/**
	 * \fn static CoreboardAction TUI_catch_event(Info * const info)
	 * \brief Catches keyboard events.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 * \param info \a Info object.
	 * \return \a CoreboardAction user-level action.
	 */
	static CoreboardAction TUI_catch_event(Info * const info);

	/**
	 * \fn static void TUI_act(Info * const info, const CoreboardAction action)
	 * \brief Alters low-level game data structures and refreshes the TUI.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 * \param param desc
	 * \return value desc
	 */
	static void TUI_act(Info * const info, const CoreboardAction action);

	/**
	 * \fn void TUI_run(void)
	 * \brief Runs the TUI.
	 * \author Sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-06-14 Sun 07:31 AM
	 */
	void TUI_run(void);

#endif
